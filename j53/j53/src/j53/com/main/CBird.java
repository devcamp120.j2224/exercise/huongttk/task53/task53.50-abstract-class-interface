package j53.com.main;

public class CBird extends CPet implements IFlyable {
    String name = "";
	String color = "";
	int age = 0;
	public CBird (String name,String color,int age ){
		this.name = name;
		this.color = color;
		this.age = age;
	}
	@Override
	public void fly() {
		System.out.println("Bird flying");
	}
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Bird sound...");
	}
}
