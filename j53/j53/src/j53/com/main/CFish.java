package j53.com.main;

import j53.com.inter.ISwimable;

public class CFish extends CPet implements ISwimable {
	String name = "";
	String color = "";
	int age = 0;
	public CFish (String name,String color,int age ){
		this.name = name;
		this.color = color;
		this.age = age;
	}

	@Override
	public void swim() {
		System.out.println("fish swiming.");
	}
	public void underWater(){
		System.out.println("tôi có thể sống dưới nước");
	}
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Fish sound...");
	}
	@Override
	public void print(){
		System.out.println("Fish print");
	}
}
