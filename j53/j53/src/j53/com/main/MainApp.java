package j53.com.main;

import java.util.ArrayList;

public class MainApp {
    public static void main(String[] args) throws Exception {
        CPet goldFish = new CFish("Gold Fish","Yellow",3);
        CPet nestBird = new CBird("Nest Brird","white",10);
        CPet tomCat = new CCat("Tom","blue",11);
        nestBird.animalSound();
        goldFish.animalSound();
        ArrayList<CPet> mArrayList = new ArrayList<CPet>();
        mArrayList.add(goldFish);
        mArrayList.add(nestBird);
        mArrayList.add(tomCat);
        CPerson person1 = new CPerson(3,20,"Toàn","Đinh",mArrayList);
        System.out.println(person1);
    }
    
}
